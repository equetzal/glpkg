# Glpkg

Instalador gráfico de lpkg al estilo [gdebi](https://launchpad.net/gdebi)

## Dependencias
- [`lpkg`](https://gitlab.com/loc-os_linux/updates/-/blob/main/LOC-OS-LPKG-10.1-x86_64.Installer)
- [`gksu`](https://www.nongnu.org/gksu) o [`zenity`](https://gitlab.gnome.org/GNOME/zenity)

## Instalación
```bash
cd /opt/Loc-OS-LPKG/
sudo mkdir glpkg && sudo chown $USER glpkg
git clone https://gitlab.com/Anonyzard/glpkg.git
```

## Uso
### Desde la terminal
`/opt/Loc-OS/glpkg/glpkg <archivo.lpkg>`

### En "Abrir con" de PCManFM
`/opt/Loc-OS/glpkg/glpkg %f`


## Licencia
Todo este proyecto está licenciado bajo la GPLv3
