#!/bin/python3
# Strings for translate
# English Translate by Anonyzard

# Buttons
sinstalar="Install"
sreinstalar="Reinstall"
sdesinstalar="Uninstall"

# Labels
spaquete="Package"
sversion="Version"
smantenedor="Maintainer"
slicencia="License"
shomepage="Homepage"
sstatus="Status"
sdetalles="Details"
shomepage="Homepage"
sirweb="Go to website"
sirindex="Go to the lpkg index website"
spassword="Enter your password"

# Statuses
sinstalado="Installed"
snoinstalado="Not installed"
sinstalando="Installing"
sdesinstalando="Uninstalling"
socupado="Glpkg is already busy, please close the other instance first"