# Seleccionar idioma
# Select language
import subprocess
lang=str(subprocess.getoutput("echo $LANG"))
if "es" in lang:
    from glpkg_lang_es import *
elif "it" in lang:
    from glpkg_lang_it import *
elif "pt_BR" in lang:
    from glpkg_lang_pt_BR import *
elif "pt_PT" in lang:
    from glpkg_lang_pt import *
else:
    from glpkg_lang_en import *
