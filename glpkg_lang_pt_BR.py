#!/bin/python3
# Strings para traducir
# Português do Brasil traduzido por Nico (Locos por Linux)

# Botoẽs
sinstalar="Instalar"
sreinstalar="Reinstalar"
sdesinstalar="Deletar"

# Etiquetas
spaquete="Pacote"
sversion="Versão"
smantenedor="Mantenedor"
slicencia="Licencia"
shomepage="Site"
sstatus="Estado"
sdetalles="Detalhes"
shomepage="Site"
sirweb="Ir ao site"
sirindex="Ir ao site do lpkg index"
spassword="Digite a sua senha"

# Estados
sinstalado="Instalado"
snoinstalado="Não instalado"
sinstalando="Instalando"
sdesinstalando="Deletando"
socupado="Glpkg já está sendo ocupado, feche a outra instância primeiro"
